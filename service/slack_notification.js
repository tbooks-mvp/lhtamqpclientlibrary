/**
 * Created by Sameerk on 30-11-2017.
 */
const slackNotifier=require ('slack-notifier');
const LHTWebLogs = require('../utils/LHTWebLogs');
const Constant = require('../common/constants');

function sendNotificationToSlack(message,webHookUrl,notificationTitle,notifyTo,iconUrl,iconEmoji) {
    try {
        slackNotifier.send(message, {
            url: webHookUrl,
            username: notificationTitle,
            channel: notifyTo,
            icon_url: iconUrl,
            icon_emoji: iconEmoji
        });
    }
    catch (err)
    {
        LHTWebLogs.LHTWebLogs(err, Constant.LOG_ERROR);
    }
}
module.exports = {sendNotificationToSlack};