/**
 * Created by deployment on 28/02/17.
 */
module.exports = {

    ONE_TO_ONE_QUEUE: 'one_to_one_queue',
    DISTRIBUTED_QUEUE: 'distributed_queue',
    PUBLISHER_SUBSCRIBER_QUEUE: 'publisher_subscriber_queue',
    ROUTING_QUEUE: 'routing_queue',
    TOPICS_QUEUE: 'topics_queue',
    REQUEST_REPLY_QUEUE: 'request_reply_queue',
    NO_CONNECTION : 'Server is not running. Restart your app',
    LOG_ERROR : 'error',
    LOG_INFO : 'info',
    RABBITMQ_NOT_STARTED : 'Unable to start Rabbit Mq server',
    RABBITMQ_START : 'RabbitMq server successfully started',


}