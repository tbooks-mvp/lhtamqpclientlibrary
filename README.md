# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Using this repository you can connect to RabbitMq server.
* Version
* [Learn more](http://rabbitmq.com)

######################## Setup instructions : ########################################

* Dependencies : requires 3 dependencies to run this module
                 -- Co library - npm install co
                 -- amqp libarary --npm install amqp --save
                 --slack-notifier --npm install slack-notifier --save


#### Deployment instructions(*) : To deplopy this into your project need some steps :

 * Just After server is started need to call one method (assumming that ampqClientLibrary is installed in your project).
 * require('amqpClicentLibrary/index').conn(AMQP_HOST_URL).
 * "amqpClicentLibrary/index" means require index file from amqpClientLibrary folder to pass your AMQP host url in .conn() method.
 * AMQP_HOST_URL should be from the Config environment.
 * This will help in creating global RabbitMq connection for your project.
 * To send slack notification on connection blocking and Unblocking followng env variable need to set:
 SLACK_WEBHOOK_URL(defalt is set to "montior_for_server" channel of lewayhertz) or SLACK_NOTIFIER_NAME
 PROJECT_NAME
 ENV
 PROJECT_ICON_URL or (UNBLOCKED_ICON_EMOJI & BLOCKED_ICON_EMOJI)



######################## Producer instructions: #########################################

 * Just require "index.js" from amqpClientLibrary where ever you want to send any data.
 * To Produce messages from your project need to call one method :
 * insertInQueue(exchangeName, queueName, replyQueue, topicKey, routingKey, replyKey, requestKey, exchangeType, queueType, queueData, isDurable = true,
   isPersistent = true, isExclusive = false, isAutoDelete = false, isNoAck = true).
 * queueData will always be send as OBJECT.
 * This insertInQueue function  will return a promise.
 * If return value is "true" then message has been send successfully otherwise show error message.


######################## Consumer instructions: ########################################$

 * Just require "index.js" from amqpClientLibrary where ever you want to recieve any data.
 * To consume messages from RabbitMq needs to call one method :
 *  getFromQueue(exchangeName, queueName, exchangeType, queueType, callback, routingKey, requestKey, isDurable = true, isPersistent = true, isExclusive = false,
    isAutoDelete = false, isNoAck = true).
 * This function will return callback.
 * Data recieved will be in string .






