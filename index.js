"use strict";
const QueueConsumer = require('./modules/consumer/queue.consumer');
const QueueProducer = require('./modules/producer/queue.producer');
const rabbitConnect = require('./config/rabbitMqConnect');
const Constants = require('./common/constants');

let connection;

module.exports = {
    conn: async function(hostUrl,startServer=true) {
        connection = await rabbitConnect.connectRabbit(hostUrl,startServer);
        if (connection)
            return true;
        return connection;
    },
    insertInQueue: async function(exchangeName, queueName, replyQueue, topicKey, routingKey, replyKey, requestKey, exchangeType, queueType, queueData, isDurable = true, isPersistent = true, isExclusive = false, isAutoDelete = false, isNoAck = true) {

        var queueObject = {
            exchangeName: exchangeName,
            queueName: queueName,
            replyQueue: replyQueue,
            exchangeType: exchangeType,
            routingKey: routingKey,
            topicKey: topicKey,
            replyKey: replyKey,
            requestKey: requestKey,
            queueType: queueType,
            queueData: queueData,
            isDurable: isDurable,
            isPersistent: isPersistent,
            isExclusive: isExclusive,
            isAutoDelete: isAutoDelete,
            isNoAck: isNoAck
        };
        //check if connection is not avilable
        if (!connection)
            return Constants.NO_CONNECTION;

        var insertInQueueResponse = await QueueProducer.insertInQueue(connection, queueObject).catch(function(err) {
            return Promise.resolve();
        });
        return Promise.resolve(insertInQueueResponse);
    },

    getFromQueue: async function (exchangeName, queueName, exchangeType, queueType, callback, routingKey, requestKey, isDurable = true, isPersistent = true, isExclusive = false, isAutoDelete = false, isNoAck = true, prefetchNumber = 1) {

        var queueObject = {
            exchangeName: exchangeName,
            queueName: queueName,
            exchangeType: exchangeType,
            routingKey: routingKey,
            queueType: queueType,
            requestKey: requestKey,
            isDurable: isDurable,
            isPersistent: isPersistent,
            isExclusive: isExclusive,
            isAutoDelete: isAutoDelete,
            isNoAck: isNoAck,
            prefetchNumber: prefetchNumber
        };
        //check if connection is not avilable
        if (!connection)
            return Constants.NO_CONNECTION;

        await QueueConsumer.getFromQueue(queueObject, connection, callback);
    }
};



