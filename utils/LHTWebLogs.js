/**
 * Created by kamalsingh on 03/03/17.
 */

function LHTWebLogs(msg, type) {
    type = type || 'info';
    console.log({message: msg, type: type});
}
module.exports = {LHTWebLogs};