const amqp = require('amqplib');
const LHTWebLogs = require('../utils/LHTWebLogs');
const Constant = require('../common/constants');
const slackNotification = require('../service/slack_notification');

module.exports = {
    connectRabbit: async function(hostUrl,startServer=true) {
        if(!startServer)
            return false;
        var me = this;
        var connection = await amqp.connect(hostUrl).catch(function (err) {
            LHTWebLogs.LHTWebLogs(err, Constant.LOG_ERROR);
            LHTWebLogs.LHTWebLogs(Constant.RABBITMQ_NOT_STARTED, Constant.LOG_INFO);
            me.connectRabbit(hostUrl,startServer);
            return false;
        });
            connection.on('blocked', function (err) {
                slackNotification.sendNotificationToSlack("RabbitMq Server-" + err + ". AMQP Connection has been BLOCKED.It does not accept new publishes.",process.env.SLACK_WEBHOOK_URL||'https://hooks.slack.com/services/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',process.env.PROJECT_NAME + ' - ' + process.env.ENV,process.env.SLACK_NOTIFIER_NAME,process.env.PROJECT_ICON_URL,process.env.BLOCKED_ICON_EMOJI);
            });
            connection.on('unblocked', function () {
                slackNotification.sendNotificationToSlack("AMQP Connection has been UNBLOCKED and now accepts publishes.",process.env.SLACK_WEBHOOK_URL||'https://hooks.slack.com/services/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',process.env.PROJECT_NAME + ' - ' + process.env.ENV,process.env.SLACK_NOTIFIER_NAME,process.env.PROJECT_ICON_URL,process.env.UNBLOCKED_ICON_EMOJI);
            });
            // Connection restart
        if (connection)
            LHTWebLogs.LHTWebLogs(Constant.RABBITMQ_START, Constant.LOG_INFO);
        else
            this.connectRabbit(hostUrl,startServer);
        return connection;
    }
};




