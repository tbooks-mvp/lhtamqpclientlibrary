/**
 * Created by deployment on 28/02/17.
 */
const Constant = require('../../common/constants');

module.exports = {

    getFromQueue: async function (queueObject, connection, callback) {

        const _this = this;
        switch (queueObject.queueType) {

            case Constant.ONE_TO_ONE_QUEUE:
                _this.oneToOneCommunication(queueObject, connection, callback);
                break;
            case Constant.DISTRIBUTED_QUEUE:
                _this.distributedCommunication(queueObject, connection, callback);
                break;
            case Constant.PUBLISHER_SUBSCRIBER_QUEUE:
                _this.publisherSubscriberCommunication(queueObject, connection, callback);
                break;
            case Constant.ROUTING_QUEUE:
                _this.routingBasedCommunication(queueObject, connection, callback);
                break;
            case Constant.TOPICS_QUEUE:
                _this.topicBasedCommunication(queueObject, connection, callback);
                break;
            case Constant.REQUEST_REPLY_QUEUE:
                _this.requestReplyCommunication(queueObject, connection, callback);
                break;
        }
    },
    oneToOneCommunication: async function (queueObject, connection, callback) {

        try {
            var channel = await connection.createChannel();
            await channel.assertQueue(queueObject.queueName, {durable: queueObject.isDurable});
            channel.consume(queueObject.queueName, function (message) {
                if (message !== null) {
                    var queueData = message.content.toString();
                    callback(null, queueData);
                }
            }, {noAck: queueObject.isNoAck});
        } catch (err) {
            return callback(err, null);
        }

    },

    distributedCommunication: async function (queueObject, connection, callback) {

        //TODO check before implementing
        //var channel = await connection.createChannel();
        //channel.prefetch(1);
        //
        // channel.consume(queueObject.queueName, function (message) {
        //    var secs = message.content.toString().split('.').length - 1;
        //
        //    var queueData = JSON.parse(message.content.toString());
        //    callback(queueData);
        //    setTimeout(function () {
        //
        //        channel.ack(message);
        //    }, secs * 1000);
        //
        //}, {noAck: queueObject.isNoAck});


    },
    publisherSubscriberCommunication: async function (queueObject, connection, callback) {

        try {
            var channel = await connection.createChannel();
            channel.prefetch(queueObject.prefetchNumber);
            await channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
            channel.assertQueue(queueObject.queueName, {exclusive: queueObject.isExclusive}).then(function (q) {
                channel.bindQueue(q.queue, queueObject.exchangeName, '', {persistent: queueObject.isPersistent});
                channel.consume(q.queue, function (message) {
                    channel.ack(message);
                    if (message !== null) {
                        var queueData = message.content.toString();
                        callback(null, queueData);
                    }
                }, {noAck: false});
            });
        } catch (err) {
            return callback(err, null);
        }
    },

    routingBasedCommunication: async function (queueObject, connection, callback) {

        //TODO check before implementing
        //var channel = await connection.createChannel();
        //var args = process.argv.slice(2);
        //var routingKey = queueObject.routingKey;
        //
        //if (args.length == 0) {
        //    process.exit(1);
        //}
        //var assertExchangeResponse = channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
        //
        //return channel.assertQueue(queueObject.queueName, {exclusive: queueObject.isExclusive}).then(function (q) {
        //
        //    args.forEach(function (routingKey) {
        //        channel.bindQueue(q.queue, queueObject.exchangeName, routingKey);
        //    });
        //
        //    channel.consume(q.queue, function (message) {
        //        console.log(" [x] %s: '%s'", message.fields.routingKey, msg.content.toString());
        //        var queueData = JSON.parse(message.content.toString());
        //    }, {noAck: queueObject.isNoAck});
        //});

    },
    topicBasedCommunication: async function (queueObject, connection, callback) {

        //TODO check before implementing
        //var channel = await connection.createChannel();
        //var args = process.argv.slice(2);
        //var assertExchange = channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
        //
        //return channel.assertQueue(queueObject.queueName, {exclusive: queueObject.isExclusive}).then(function (q) {
        //
        //    args.forEach(function (key) {
        //        channel.bindQueue(q.queue, queueObject.exchangeName, key);
        //    });
        //
        //    channel.consume(q.queue, function (message) {
        //        console.log(" [x] %s: '%s'", message.fields.routingKey, msg.content.toString());
        //        var queueData = JSON.parse(message.content.toString());
        //    }, {noAck: queueObject.isNoAck});
        //});

    },
    requestReplyCommunication: async function (queueObject, connection, callback) {

        //TODO in suppose to do in AMQP lib to handle RPC acknowledgement due to short time relese on  work around
        let channel = await connection.createChannel();
        var q = queueObject.queueName;
        let assertQueueResponse = await channel.assertQueue(queueObject.queueName, {durable: true});
        // channel.prefetch(1);
        sendAndConsume(0, assertQueueResponse, queueObject, channel, callback);


        //TODO check before implementing
        //var channel = await connection.createChannel();
        //
        //channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {
        //    durable: queueObject.isDurable,
        //    autoDelete: queueObject.isAutoDelete
        //});
        //channel.assertQueue(queueObject.queueName, {
        //    durable: queueObject.isDurable,
        //    autoDelete: queueObject.isAutoDelete,
        //    exclusive: queueObject.isExclusive
        //});
        //channel.bindQueue(queueObject.queueName, queueObject.exchangeName, queueObject.requestKey);
        //return channel.consume(queueObject.queueName, function (message) {
        //
        //    //callback function on receiving messages, reply to the reply_to header
        //
        //    console.log(message.content.toString());
        //    channel.publish(queueObject.exchangeName, message.properties.replyTo, new Buffer("Reply to " + message.content.toString()), options = {
        //        contentType: "text/plain",
        //        deliveryMode: 1
        //    }, function (err, ok) {
        //        if (err != null) {
        //            channel.nack(message);
        //        }
        //        else {
        //            channel.ack(message);
        //        }
        //    });
        //}, {noAck: queueObject.isNoAck});
    }
};

var sendAndConsume = async function (err, q, queueObject, channel, callback) {
    channel.consume(queueObject.queueName, function reply(msg) {
        callback(channel, msg);
        channel.ack(msg);
    });

};

/*

 channel.sendToQueue(msg.properties.replyTo,
 new Buffer(getDataResponse.toString()),
 {correlationId: msg.properties.correlationId});

 channel.ack(msg);
 */
