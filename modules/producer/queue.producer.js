/**
 * Created by deployment on 28/02/17.
 */
const Constant = require('../../common/constants');

module.exports = {

    insertInQueue: async function (connection, queueObject) {

        const _this = this;
        switch (queueObject.queueType) {
            case Constant.ONE_TO_ONE_QUEUE:
                return await _this.oneToOneCommunication(connection, queueObject);
                break;
            case Constant.DISTRIBUTED_QUEUE:
                return await _this.distributedCommunication(connection, queueObject);
                break;
            case Constant.PUBLISHER_SUBSCRIBER_QUEUE:
                return await _this.publisherSubscriberCommunication(connection, queueObject);
                break;
            case Constant.ROUTING_QUEUE:
                return await _this.routingBasedCommunication(connection, queueObject);
                break;
            case Constant.TOPICS_QUEUE:
                return await _this.topicBasedCommunication(connection, queueObject);
                break;
            case Constant.REQUEST_REPLY_QUEUE:
                return await _this.requestReplyCommunication(connection, queueObject);
                break;
        }
    },

    oneToOneCommunication: async function(connection, queueObject) {
        try {
            var channel = await connection.createChannel();

            //TODO remove args and check what happen and implement it in correct way

            var message = process.argv.slice(2).join(' ') || queueObject.queueData;
            // var message = process.argv.slice(2).join(' ') || JSON.stringify(queueObject.queueData);
            await channel.assertQueue(queueObject.queueName, {durable: queueObject.isDurable});
            return channel.sendToQueue(queueObject.queueName, new Buffer(message), {persistent: queueObject.isPersistent});
        } catch (err) {
            return false;
        }

    },

    distributedCommunication: async function(connection, queueObject) {

        //TODO check before implementing
        //var channel = connection.channel;
        //var message = process.argv.slice(2).join(' ') || JSON.stringify(queueObject.queueData);
        //var assertQueueResponse = await channel.assertQueue(queueObject.queueName, {durable: queueObject.isDurable});
        //
        //var sendToQueueResponse = channel.sendToQueue(queueObject.queueName, new Buffer(message), {persistent: queueObject.isPersistent});
        //// console.log("message send", msgResponse);
        //return sendToQueueResponse;


    },
    publisherSubscriberCommunication: async function(connection, queueObject) {

        try {
            var channel = await connection.createChannel();
            var message = process.argv.slice(2).join(' ') || JSON.stringify(queueObject.queueData);
            await channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
            var publishResponse = channel.publish(queueObject.exchangeName, '', new Buffer(message), {persistent: queueObject.isPersistent});
            if (publishResponse)
                channel.close();
            return publishResponse;

        } catch (err) {
            return false;
        }

    },
    routingBasedCommunication: async function(connection, queueObject) {
        //TODO in suppose to do in AMQP lib to handle routing based communication due to short time relese on  work around
        try {
            var channel = await connection.createChannel();
            var args = process.argv.slice(2);
            var message = args.slice(1).join(' ') || JSON.stringify(queueObject.queueData);
            var routingKey = (args.length > 0) ? args[0] : queueObject.routingKey;
            var assertExchangeResponse = await channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
            var publishMessageResponse = channel.publish(queueObject.exchangeName, routingKey, new Buffer(message), {persistent: queueObject.isPersistent});
            if (publishMessageResponse)
                channel.close();
            return publishMessageResponse;

        } catch (err) {
            return false;
        }
    },
    topicBasedCommunication: async function(connection, queueObject) {

        //TODO check before implementing
        //var channel = connection.channel;
        //
        //var args = process.argv.slice(2);
        //var message = args.slice(1).join(' ') || JSON.stringify(queueObject.queueData);
        //var topicKey = (args.length > 0) ? args[0] : queueObject.topicKey;
        //
        //var assertExchangeResponse = await channel.assertExchange(queueObject.exchangeName, queueObject.exchangeType, {durable: queueObject.isDurable});
        //var publishMessageResponse = channel.publish(queueObject.exchangeName, topicKey, new Buffer(message), {persistent: queueObject.isPersistent});
        //
        //return publishMessageResponse;

    },
    requestReplyCommunication: async function(connection, queueObject) {
//TODO in suppose to do in AMQP lib to handle RPC acknowledgement due to short time relese on  work around
        let channel = await connection.createChannel();
        let message = queueObject.queueData;
        // var ex = 'topic_logs';
        // channel.assertExchange(ex, 'topic', {durable: false});

        let assertQueueResponse = await channel.assertQueue(queueObject.queueName, {
            durable: false,
            //    autoDelete: queueObject.isAutoDelete,
            exclusive: false
        });
        sendAndConsume(0, assertQueueResponse, queueObject, channel, message, connection);


        //TODO check before implementing
        //var channel = connection.channel;
        //
        //var message = JSON.stringify(queueObject.queueData);
        //
        //var assertQueueResponse = await channel.assertQueue(queueObject.queueName, {
        //    durable: queueObject.isDurable,
        //    autoDelete: queueObject.isAutoDelete,
        //    exclusive: queueObject.isExclusive
        //});
        //var bindQueueResponse = channel.publish(queueObject.queueName, queueObject.exchangeName, queueObject.replyKey);
        //var consumeQueueResponse = channel.consume(queueObject.replyQueue, function (message) {
        //    console.log(message.content.toString());
        //}, {noAck: true});
        //
        ////send the request message after 1 second
        //setTimeout(function () {
        //    channel.publish(queueObject.exchangeName, queueObject.requestKey, new Buffer(message), options = {
        //        contentType: "text/plain",
        //        deliveryMode: 1,
        //        replyTo: queueObject.replyKey
        //    }, function (err, ok) {
        //        if (err != null) {
        //            console.error("Error: failed to send message\n" + err);
        //        }
        //    });
        //}, 1000);
        //
        //return consumeQueueResponse;

    },
};

function generateUuid() {
    return ((Math.floor(Math.random() * 201) * 226) +
    (Math.floor(Math.random() * 201) * 235) +
    (Math.floor(Math.random() * 201) * 247)).toString();
}

function sendAndConsume(err, q, queueObject, channel, message, connection) {
    let correlationId = generateUuid();
    channel.consume(q.queue, function (message) {

        if (message.properties.correlationId = correlationId) {
            console.log(message.content.toString());
            setTimeout(function () {
            }, 500);
        }
    }, {noAck: false});

    try {
        channel.sendToQueue(queueObject.queueName,
            new Buffer(message.toString()),
            {correlationId: correlationId, replyTo: q.queue});
    }
    catch (err) {
        console.log(err)
    }
}
